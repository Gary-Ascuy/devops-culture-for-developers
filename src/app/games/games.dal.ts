import { v4 as uuidv4 } from 'uuid'

export const db: { [name: string]: Game } = {} // in-memory store

export interface Game {
    _id?: string,
    name?: string,
    year?: string,
    starts?: number,
}

export function getBy(id: string): Game {
    return db[id]
}

export function getAll(): Game[] {
    return Object.values(db)
}

export function create(game: Game): Game {
    game._id = game._id || uuidv4()
    db[game._id] = game
    return game
}

export function update(game: Game): Game {
    if (!game._id) throw new Error('Invalid Data')
    db[game._id] = game
    return game
}

export function remove(id: string): Game {
    const deleted = db[id]
    delete db[id]
    return deleted
}
