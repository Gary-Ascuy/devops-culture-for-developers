import express from 'express'

import { apiRoutes } from './api'
import { homeRoutes } from './home/home.controller'

export const controllers = express.Router()

controllers.use('/api/v1', apiRoutes)
controllers.use('/', homeRoutes)
