import { Router } from 'express'

import { gameRoutes } from './games/games.controller'

export const apiRoutes = Router()
apiRoutes.use('/games', gameRoutes)
