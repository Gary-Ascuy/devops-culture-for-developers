import { NextFunction, Request, Response } from 'express'

// TODO: Improve to return a better error according to exception 
export function errorHandler(error: any, req: Request, res: Response, next: NextFunction) {
  res.status(500).send({ code: 500, error: 'Unexpected Error. Please contact your Administrator' })
  next()
}
